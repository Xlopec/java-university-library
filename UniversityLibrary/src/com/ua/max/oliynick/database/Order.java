package com.ua.max.oliynick.database;

import java.io.Serializable;

/**
 * This class represents
 * database table, name
 * of which "order"
 * @version 1.0
 * @author Max Oliynick
 * */

public final class Order implements Serializable {
	
	private static final long serialVersionUID = 7641507890598971567L;
	
	/**name of the database table*/
	public static final String TABLE_TITLE = "journal";
	
	public static final String ID_COLUMN = "idOrder";
	public static final String SINCE_DATE_COLUMN = "since_date";
	public static final String TO_DATE_COLUMN = "to_date";
	public static final String REAL_DATE_COLUMN = "real_date";
	public static final String BOOK_REF_COLUMN = "Book_Instance_inv_num";
	public static final String USER_REF_COLUMN = "User_reader_card";
	
	private int id;
	private long since_date;
	private long to_date;
	private long real_date;
	
	/**Reference to the book*/
	private BookInstance bookInstance;
	/**Reference to the user*/
	private User user;

	public Order() {}
	
	public Order(long since_date, long to_date, long real_date,
			BookInstance bookInstance, User user) {
		super();
		this.since_date = since_date;
		this.to_date = to_date;
		this.real_date = real_date;
		this.bookInstance = bookInstance;
		this.user = user;
	}
	
	public Order(long since_date, long to_date,
			BookInstance bookInstance, User user) {
		super();
		this.since_date = since_date;
		this.to_date = to_date;
		this.bookInstance = bookInstance;
		this.user = user;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) return false;
		if(!(obj instanceof Order)) return false;
		
		Order another = (Order) obj;
		
		if(another.getId() != getId()) return false;
		if(another.getSince() != getSince()) return false;
		if(another.getTo() != getTo()) return false;
		if(another.getRealDate() != getRealDate()) return false;
		
		return super.equals(another);
	}

	@Override
	public int hashCode() {
		return id * 37;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public long getSince() {
		return since_date;
	}

	public void setSinceDate(long since_date) {
		this.since_date = since_date;
	}

	public long getTo() {
		return to_date;
	}

	public void setToDate(long to_date) {
		this.to_date = to_date;
	}

	public long getRealDate() {
		return real_date;
	}

	public void setRealDate(long real_date) {
		this.real_date = real_date;
	}

	public BookInstance getBookInstance() {
		return bookInstance;
	}

	public void setBookInstance(BookInstance bookInstance) {
		this.bookInstance = bookInstance;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	@Override
	public String toString() {
		return String.valueOf(getId()) + ' ' 
				+ String.valueOf(getSince()) + ' ' 
				+ String.valueOf(getTo()) + ' ' 
				+ String.valueOf(getRealDate()) + ' ' 
				+ String.valueOf(getBookInstance().getInventoryNum()) + ' '
				+ String.valueOf(getUser().getReaderCard());
	}

}
