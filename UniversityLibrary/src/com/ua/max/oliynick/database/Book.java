package com.ua.max.oliynick.database;

import java.io.Serializable;

/**
 * This class represents
 * database table, name
 * of which "book"
 * @version 1.0
 * @author Max Oliynick
 * */

public final class Book implements Serializable {
	
	private static final long serialVersionUID = -2134647897063865409L;

	/**name of the database table*/
	public static final String TABLE_TITLE = "book";
	
	public static final String ID_COLUMN = "ISBN";
	public static final String TITLE_COLUMN = "title";
	public static final String PUB_HOUSE_COLUMN = "pub_house";
	public static final String PUB_YEAR_COLUMN = "pub_year";
	public static final String PAGES_COLUMN = "pages";
	public static final String PRICE_COLUMN = "price";
	public static final String INSTANCES_COLUMN = "instances";
	
	/**
	 * The card of reader which is
	 * used as primary key
	 *  */
	
	private long isbn;
	private String title;
	private String publishingHouse;
	private int publishingYear;
	private int pages = -1;
	private int price;
	private int instances;
	
	public Book() {}
	
	public Book(String title, String publishingHouse,
			int publishingYear, int pages, int price) {
		super();
		this.title = title;
		this.publishingHouse = publishingHouse;
		this.publishingYear = publishingYear;
		this.pages = pages;
		this.price = price;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) return false;
		if(!(obj instanceof Book)) return false;
		
		Book another = (Book) obj;
		
		if(another.getIsbn() != getIsbn()) return false;
		
		if(another.getTitle() != null && getTitle() !=null) {
			if(!another.getTitle().equals(getTitle())) return false;
		}
		if(another.getPublishingHouse() != null && getPublishingHouse() != null) {
			if(!another.getPublishingHouse().equals(getPublishingHouse())) return false;
		}
		
		if(another.getPages() != getPages()) return false;
		if(another.getPrice() != getPrice()) return false;
		
		return super.equals(another);
	}

	@Override
	public int hashCode() {
		return (int) (isbn * 29);
	}

	public long getIsbn() {
		return isbn;
	}
	public void setIsbn(long isbn) {
		this.isbn = isbn;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPublishingHouse() {
		return publishingHouse;
	}
	public void setPublishingHouse(String publishingHouse) {
		this.publishingHouse = publishingHouse;
	}
	public int getPublishingYear() {
		return publishingYear;
	}
	public void setPublishingYear(int publishingYear) {
		this.publishingYear = publishingYear;
	}
	public int getPages() {
		return pages;
	}
	public void setPages(int pages) {
		this.pages = pages;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}

	public int getInstances() {
		return instances;
	}

	public void setInstances(int instances) {
		this.instances = instances;
	}
	
	@Override
	public String toString() {
		return String.valueOf(getIsbn()) + ' ' 
				+ getTitle() + ' ' 
				+ getPublishingHouse() + ' ' 
				+ String.valueOf(getPublishingYear()) + ' ' 
				+ String.valueOf(getPages()) + ' ' 
				+ String.valueOf(getPrice()) + ' ' 
				+ String.valueOf(getInstances());
	}

}
