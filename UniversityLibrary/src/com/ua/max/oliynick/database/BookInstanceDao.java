package com.ua.max.oliynick.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Assert;

import com.ua.max.oliynick.interfaces.Dao;
import com.ua.max.oliynick.interfaces.ObjectMapper;

/**
 * Implementation of {@link Dao} class
 * for {@link BookInstance} class
 * @author Max Oliynick
 * @version 1.0
 * */

public class BookInstanceDao extends Dao < BookInstance > {
	
	/**Fields to map database object*/
	public final static String[] MAPPING_FIELDS = new String [] 
			{
			BookInstance.ID_COLUMN,
			BookInstance.STATUS_COLUMN,
			BookInstance.APPEAR_DATE_COLUMN,
			BookInstance.BOOK_ID_COLUMN
			};
	
	// Maps database request into java object
	private final static ObjectMapper<BookInstance> OBJECT_MAPPER = new ObjectMapper<BookInstance>() {
		
		public List<BookInstance> map(final ResultSet rs, final String [] columns) throws SQLException {
			
			List<BookInstance> items = new ArrayList<BookInstance>();
	    	
	    	while(rs.next()) {
	    		
	    		BookInstance item = new BookInstance();
	    		
	    		for(int i = 0; i < columns.length; ++i) {
	    			if(columns[i].equals(BookInstance.ID_COLUMN)) {
	        			item.setInventoryNum(rs.getInt(BookInstance.ID_COLUMN));
	        			
	        		} else if(columns[i].equalsIgnoreCase(BookInstance.STATUS_COLUMN)) {
	        			item.setStatus(BookStatus.values()[rs.getShort(BookInstance.STATUS_COLUMN)]);
	        			
	        		} else if(columns[i].equalsIgnoreCase(BookInstance.APPEAR_DATE_COLUMN)) {
	        			item.setAppearanceDate(rs.getLong(BookInstance.APPEAR_DATE_COLUMN));
	        			
	        		} else if(columns[i].equalsIgnoreCase(BookInstance.BOOK_ID_COLUMN)) {
	        			Book bookRef = new Book();
	        			bookRef.setIsbn(rs.getLong(BookInstance.BOOK_ID_COLUMN));
	        			item.setBookRef(bookRef);
	        			
	        		}
	    		}
	    		
	    		items.add(item);
	    	}
	    	
	    	return items;
		}
	};


	/**Creates dao for {@link BookInstance} class
	 * @param connection - database connection to use
	 * @param mapper - mapper to map objects*/
	public BookInstanceDao(final Connection connection) {
		super(connection);
	}

	@Override
	public synchronized List<BookInstance> all() throws SQLException {
		
		StringBuilder query = new StringBuilder("SELECT * FROM ");
		query.append(BookInstance.TABLE_TITLE);
		
		Statement statement = null;
		ResultSet resultSet = null;
		List<BookInstance> entries = null;
		
		try {
			statement = super.connection.createStatement();
			resultSet = statement.executeQuery(query.toString());
			entries = BookInstanceDao.OBJECT_MAPPER.map(resultSet, BookInstanceDao.MAPPING_FIELDS);
			
		} finally {
			if(statement != null) statement.close();
			if(resultSet != null) resultSet.close();
		}
		
		return entries;
	}
	
	@Override
	public synchronized int create(BookInstance instance) throws SQLException {
		
		if(instance == null)
			throw new NullPointerException("instance == null");
		
		StringBuilder query = new StringBuilder("INSERT INTO ");
		query.append(BookInstance.TABLE_TITLE);
		query.append(" SET ");
		
		query.append(BookInstance.STATUS_COLUMN);
		query.append("=?,");
		query.append(BookInstance.APPEAR_DATE_COLUMN);
		query.append("=?,");
		query.append(BookInstance.BOOK_ID_COLUMN);
		query.append("=?");
		
		PreparedStatement statement = null;
		
		try {
		
			statement =	super.connection.prepareStatement(query.toString());
			statement.setShort(1, (short) instance.getStatus().ordinal());
			statement.setLong(2, instance.getAppearanceDate());
			statement.setLong(3, instance.getBookRef().getIsbn());
			
			return statement.executeUpdate();
			
		} finally {
			if(statement != null) statement.close();
		}
	}

	@Override
	public synchronized BookInstance read(int id) throws SQLException {
		
		StringBuilder query = new StringBuilder("SELECT * FROM ");
		query.append(BookInstance.TABLE_TITLE);
		query.append(" WHERE ");
		query.append(BookInstance.ID_COLUMN);
		query.append("=? LIMIT 1");
		
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		Collection<BookInstance> entries = null;
		
		try {
			statement = super.connection.prepareStatement(query.toString());
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			entries = BookInstanceDao.OBJECT_MAPPER.map(resultSet, BookInstanceDao.MAPPING_FIELDS);
			
		} finally {
			if(statement != null) statement.close();
			if(resultSet != null) resultSet.close();
		}
		
		// Cannot be more than 1
		// result with the same id
		Assert.assertFalse("multiple rows with such id exist", entries.size() > 1);
		
		/* 
		 * getting the first object from
		 * the collection of entries
		 * */
		BookInstance [] a = new BookInstance[1];
		entries.toArray(a);
		
		return a[0];
	}

	@Override
	public synchronized int update(BookInstance instance) throws SQLException {
		
		if(instance == null)
			throw new NullPointerException("instance == null");
		
		StringBuilder query = new StringBuilder("UPDATE ");
		query.append(BookInstance.TABLE_TITLE);
		query.append(" SET ");
		
		query.append(BookInstance.STATUS_COLUMN);
		query.append("=?,");
		query.append(BookInstance.APPEAR_DATE_COLUMN);
		query.append("=?,");
		query.append(BookInstance.BOOK_ID_COLUMN);
		query.append("=?");
		
		query.append(" WHERE ");
		query.append(BookInstance.ID_COLUMN);
		query.append("=? LIMIT 1");
		
		PreparedStatement statement = null;
		
		try {
		
			statement =	super.connection.prepareStatement(query.toString());
			statement.setShort(1, (short) instance.getStatus().ordinal());
			statement.setLong(2, instance.getAppearanceDate());
			statement.setLong(3, instance.getBookRef().getIsbn());
			
			return statement.executeUpdate();
			
		} finally {
			if(statement != null) statement.close();
		}
		
	}

	@Override
	public synchronized int delete(BookInstance instance) throws SQLException {
		
		if(instance == null)
			throw new NullPointerException("instance == null");
		
		StringBuilder query = new StringBuilder("DELETE FROM ");
		query.append(BookInstance.TABLE_TITLE);
		query.append(" WHERE ");
		query.append(BookInstance.ID_COLUMN);
		query.append("=? LIMIT 1");
		
		PreparedStatement statement = null;
		
		try {
		
			statement =	super.connection.prepareStatement(query.toString());
			statement.setInt(1, instance.getInventoryNum());
			
			return statement.executeUpdate();
			
		} finally {
			if(statement != null) statement.close();
		}
	}
	
}
