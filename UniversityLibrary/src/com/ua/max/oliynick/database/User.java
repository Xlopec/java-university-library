package com.ua.max.oliynick.database;

import java.io.Serializable;

/**
 * This class represents
 * database table, name
 * of which "user"
 * @version 1.0
 * @author Max Oliynick
 * */

public final class User implements Serializable {
	
	private static final long serialVersionUID = 1350450388070013100L;

	/**name of the database table*/
	public static final String TABLE_TITLE = "user";
	
	public static final String ID_COLUMN = "reader_card";
	public static final String NAME_COLUMN = "name";
	public static final String SURNAME_COLUMN = "surname";
	public static final String BIRTHDATE_COLUMN = "birthdate";
	

	/**
	 * The card of reader which is
	 * used as primary key
	 *  */
	private int readerCard;
	
	private String name;
	private String surname;
	private long birthdate;
	
	public User() {}
	
	public User(final String name, final String surname, long birthdate) {
		this.name = name;
		this.surname = surname;
		this.birthdate = birthdate;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) return false;
		if(!(obj instanceof Order)) return false;
		
		User another = (User) obj;
		
		if(another.getReaderCard() != getReaderCard()) return false;
		
		if(another.getName() != null && getName() != null) {
			if(!another.getName().equals(getName())) return false;
		}
		
		if(another.getSurname() != null && getSurname() != null) {
			if(!another.getSurname().equals(getSurname())) return false;
		}
		
		if(another.getBirthdate() != getBirthdate()) return false;
		
		return super.equals(another);
	}

	@Override
	public int hashCode() {
		return readerCard * 3;
	}

	public int getReaderCard() {
		return readerCard;
	}

	public void setReaderCard(int readerCard) {
		this.readerCard = readerCard;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public long getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(long birthdate) {
		this.birthdate = birthdate;
	}
	
	@Override
	public String toString() {
		return String.valueOf(getReaderCard()) + ' ' 
				+ getName() + ' ' 
				+ getSurname() + ' ' 
				+ String.valueOf(getBirthdate());
	}
	
}
