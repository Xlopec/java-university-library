package com.ua.max.oliynick.database;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Assert;

import com.ua.max.oliynick.interfaces.Dao;
import com.ua.max.oliynick.interfaces.ObjectMapper;
import com.ua.max.oliynick.utils.FileUtils;

/**
 * Implementation of {@link Dao} class
 * for {@link Book} class
 * @author Max Oliynick
 * @version 1.0
 * */

public class BookDao extends Dao < Book > {
	
	/**Fields to map database object*/
	public final static String[] MAPPING_FIELDS = new String [] 
			{
			Book.ID_COLUMN,
			Book.TITLE_COLUMN,
			Book.PUB_HOUSE_COLUMN,
			Book.PUB_YEAR_COLUMN,
			Book.PAGES_COLUMN,
			Book.PRICE_COLUMN,
			Book.INSTANCES_COLUMN
			};
	
	// Maps database request into java object
	private final static ObjectMapper<Book> OBJECT_MAPPER = new ObjectMapper<Book>() {
		
		public List<Book> map(final ResultSet rs, final String [] columns) throws SQLException {
			
			List<Book> items = new ArrayList<Book>();
	    	
	    	while(rs.next()) {
	    		
	    		Book item = new Book();
	    		
	    		for(int i = 0; i < columns.length; ++i) {
	    			if(columns[i].equals(Book.ID_COLUMN)) {
	        			item.setIsbn(rs.getLong(Book.ID_COLUMN));
	        			
	        		} else if(columns[i].equalsIgnoreCase(Book.TITLE_COLUMN)) {
	        			item.setTitle(rs.getString(Book.TITLE_COLUMN));
	        			
	        		} else if(columns[i].equalsIgnoreCase(Book.PUB_HOUSE_COLUMN)) {
	        			item.setPublishingHouse(rs.getString(Book.PUB_HOUSE_COLUMN));
	        			
	        		} else if(columns[i].equalsIgnoreCase(Book.PUB_YEAR_COLUMN)) {
	        			item.setPublishingYear(rs.getInt(Book.PUB_YEAR_COLUMN));
	        			
	        		} else if(columns[i].equalsIgnoreCase(Book.PAGES_COLUMN)) {
	        			item.setPages(rs.getInt(Book.PAGES_COLUMN));
	        			
	        		} else if(columns[i].equalsIgnoreCase(Book.PRICE_COLUMN)) {
	        			item.setPrice(rs.getInt(Book.PRICE_COLUMN));
	        			
	        		} else if(columns[i].equalsIgnoreCase(Book.INSTANCES_COLUMN)) {
	        			item.setInstances(rs.getInt("available_num"));
	        			
	        		}
	    		}
	    		
	    		items.add(item);
	    	}
	    	
	    	return items;
		}
	};
	
	private static final String SELECT_ALL_QUERY;
	
	static {
		final InputStream is = BookDao.class.getClassLoader().getResourceAsStream("com/ua/resources/select_books_all.sql");
		SELECT_ALL_QUERY = FileUtils.getStringFromInputStream(is);
	}


	/**Creates dao for {@link Book} class
	 * @param connection - database connection to use
	 * @param mapper - mapper to map objects*/
	public BookDao(final Connection connection) {
		super(connection);
	}

	@Override
	public synchronized List<Book> all() throws SQLException {
		
		Statement statement = null;
		ResultSet resultSet = null;
		List<Book> entries = null;
		
		try {
			statement = super.connection.createStatement();
			resultSet = statement.executeQuery(SELECT_ALL_QUERY);
			entries = BookDao.OBJECT_MAPPER.map(resultSet, BookDao.MAPPING_FIELDS);
			
		} finally {
			if(statement != null) statement.close();
			if(resultSet != null) resultSet.close();
		}
		
		return entries;
	}
	
	@Override
	public synchronized int create(Book instance) throws SQLException {
		
		if(instance == null)
			throw new NullPointerException("instance == null");
		
		StringBuilder query = new StringBuilder("INSERT INTO ");
		query.append(Book.TABLE_TITLE);
		query.append(" SET ");
		
		query.append(Book.TITLE_COLUMN);
		query.append("=?,");
		query.append(Book.PUB_HOUSE_COLUMN);
		query.append("=?,");
		query.append(Book.PUB_YEAR_COLUMN);
		query.append("=?,");
		query.append(Book.PAGES_COLUMN);
		query.append("=?,");
		query.append(Book.PRICE_COLUMN);
		query.append("=?");
		
		PreparedStatement statement = null;
		
		try {
		
			statement =	super.connection.prepareStatement(query.toString());
			statement.setString(1, instance.getTitle());
			statement.setString(2, instance.getPublishingHouse());
			statement.setInt(3, instance.getPublishingYear());
			statement.setInt(4, instance.getPages());
			statement.setLong(5, instance.getPrice());
			statement.setLong(6, instance.getInstances());
			
			return statement.executeUpdate();
			
		} finally {
			if(statement != null) statement.close();
		}
	}

	@Override
	public synchronized Book read(int id) throws SQLException {
		
		StringBuilder query = new StringBuilder("SELECT * FROM ");
		query.append(Book.TABLE_TITLE);
		query.append(" WHERE ");
		query.append(Book.ID_COLUMN);
		query.append("=? LIMIT 1");
		
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		Collection<Book> entries = null;
		
		try {
			statement = super.connection.prepareStatement(query.toString());
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			entries = BookDao.OBJECT_MAPPER.map(resultSet, BookDao.MAPPING_FIELDS);
			
		} finally {
			if(statement != null) statement.close();
			if(resultSet != null) resultSet.close();
		}
		
		// Cannot be more than 1
		// result with the same id
		Assert.assertFalse("multiple rows with such id exist", entries.size() > 1);
		
		/* 
		 * getting the first object from
		 * the collection of entries
		 * */
		Book [] a = new Book[1];
		entries.toArray(a);
		
		return a[0];
	}

	@Override
	public synchronized int update(Book instance) throws SQLException {
		
		if(instance == null)
			throw new NullPointerException("instance == null");
		
		StringBuilder query = new StringBuilder("UPDATE ");
		query.append(Book.TABLE_TITLE);
		query.append(" SET ");
		
		query.append(Book.TITLE_COLUMN);
		query.append("=?,");
		query.append(Book.PUB_HOUSE_COLUMN);
		query.append("=?,");
		query.append(Book.PUB_YEAR_COLUMN);
		query.append("=?,");
		query.append(Book.PAGES_COLUMN);
		query.append("=?,");
		query.append(Book.PRICE_COLUMN);
		query.append("=?");
		
		query.append(" WHERE ");
		query.append(Book.ID_COLUMN);
		query.append("=? LIMIT 1");
		
		PreparedStatement statement = null;
		
		try {
		
			statement =	super.connection.prepareStatement(query.toString());
			statement.setString(1, instance.getTitle());
			statement.setString(2, instance.getPublishingHouse());
			statement.setInt(3, instance.getPublishingYear());
			statement.setInt(4, instance.getPages());
			statement.setLong(5, instance.getPrice());
			
			return statement.executeUpdate();
			
		} finally {
			if(statement != null) statement.close();
		}
		
	}

	@Override
	public synchronized int delete(Book instance) throws SQLException {
		
		if(instance == null)
			throw new NullPointerException("instance == null");
		
		StringBuilder query = new StringBuilder("DELETE FROM ");
		query.append(Book.TABLE_TITLE);
		query.append(" WHERE ");
		query.append(Book.ID_COLUMN);
		query.append("=? LIMIT 1");
		
		PreparedStatement statement = null;
		
		try {
		
			statement =	super.connection.prepareStatement(query.toString());
			statement.setLong(1, instance.getIsbn());
			
			return statement.executeUpdate();
			
		} finally {
			if(statement != null) statement.close();
		}
	}
	
}
