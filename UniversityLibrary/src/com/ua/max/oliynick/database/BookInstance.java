package com.ua.max.oliynick.database;

import java.io.Serializable;

/**
 * This class represents
 * database table, name
 * of which "book_instance"
 * @version 1.0
 * @author Max Oliynick
 * */

public final class BookInstance implements Serializable {
	
	private static final long serialVersionUID = 6868187053772932713L;
	/**name of the database table*/
	public static final String TABLE_TITLE = "book_instance";
	
	public static final String ID_COLUMN = "inv_num";
	public static final String STATUS_COLUMN = "status";
	public static final String APPEAR_DATE_COLUMN = "app_date";
	public static final String BOOK_ID_COLUMN = "Book_ISBN";
	
	private int inventoryNum;
	private BookStatus status = BookStatus.AVAILABLE;
	private long appearanceDate;
	/**
	 * Used as a foreign key
	 * to the book this instance
	 * belongs to
	 * */
	private Book bookRef;
	
	public BookInstance() {}
	
	public BookInstance(BookStatus status,
			long appearanceDate, Book bookRef) {
		this.status = status;
		this.appearanceDate = appearanceDate;
		this.bookRef = bookRef;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) return false;
		if(!(obj instanceof Order)) return false;
		
		BookInstance another = (BookInstance) obj;
		
		if(another.getStatus() != getStatus()) return false;
		if(another.getAppearanceDate() != getAppearanceDate()) return false;
		
		if(another.getBookRef() != null && getBookRef() != null) {
			if(!another.getBookRef().equals(getBookRef())) return false;
		}
		
		return super.equals(another);
	}

	@Override
	public int hashCode() {
		return inventoryNum * 41;
	}

	public int getInventoryNum() {
		return inventoryNum;
	}
	public void setInventoryNum(int inventoryNum) {
		this.inventoryNum = inventoryNum;
	}
	public BookStatus getStatus() {
		return status;
	}
	public void setStatus(BookStatus status) {
		this.status = status;
	}
	public long getAppearanceDate() {
		return appearanceDate;
	}
	public void setAppearanceDate(long appearanceDate) {
		this.appearanceDate = appearanceDate;
	}
	public Book getBookRef() {
		return bookRef;
	}
	public void setBookRef(Book bookRef) {
		this.bookRef = bookRef;
	}
	
	@Override
	public String toString() {
		return String.valueOf(getInventoryNum()) + ' ' 
				+ String.valueOf(getAppearanceDate()) + ' ' 
				+ getStatus() + ' ' 
				+ String.valueOf(getBookRef().getIsbn());
	}

}
