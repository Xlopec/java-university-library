package com.ua.max.oliynick.database;

public enum BookStatus {
	AVAILABLE, UNAVAILABLE, DELETED
}
