package com.ua.max.oliynick.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Assert;

import com.ua.max.oliynick.interfaces.Dao;
import com.ua.max.oliynick.interfaces.ObjectMapper;

/**
 * Implementation of {@link Dao} class
 * for {@link User} class
 * @author Max Oliynick
 * @version 1.0
 * */

public class UserDao extends Dao < User > {
	
	/**Fields to map database object*/
	public final static String[] MAPPING_FIELDS = new String [] 
			{
			User.ID_COLUMN,
			User.NAME_COLUMN,
			User.SURNAME_COLUMN,
			User.BIRTHDATE_COLUMN,
			};
	
	// Maps database request into java object
	private final static ObjectMapper<User> OBJECT_MAPPER = new ObjectMapper<User>() {
		
		public List<User> map(final ResultSet rs, final String [] columns) throws SQLException {
			
			List<User> items = new ArrayList<User>();
	    	
	    	while(rs.next()) {
	    		
	    		User item = new User();
	    		
	    		for(int i = 0; i < columns.length; ++i) {
	    			if(columns[i].equals(User.ID_COLUMN)) {
	        			item.setReaderCard(rs.getInt(User.ID_COLUMN));
	        			
	        		} else if(columns[i].equalsIgnoreCase(User.NAME_COLUMN)) {
	        			item.setName(rs.getString(User.NAME_COLUMN));
	        			
	        		} else if(columns[i].equalsIgnoreCase(User.SURNAME_COLUMN)) {
	        			item.setSurname(rs.getString(User.SURNAME_COLUMN));
	        			
	        		} else if(columns[i].equalsIgnoreCase(User.BIRTHDATE_COLUMN)) {
	        			item.setBirthdate(rs.getLong(User.BIRTHDATE_COLUMN));
	        			
	        		}
	    		}
	    		
	    		items.add(item);
	    	}
	    	
	    	return items;
		}
	};

	/**Creates dao for {@link User} class
	 * @param connection - database connection to use
	 * @param mapper - mapper to map objects*/
	public UserDao(final Connection connection) {
		super(connection);
	}

	@Override
	public synchronized List<User> all() throws SQLException {
		
		Statement statement = null;
		ResultSet resultSet = null;
		List<User> entries = null;
		
		try {
			statement = super.connection.createStatement();
			StringBuilder query = new StringBuilder("SELECT * FROM ");
			query.append(User.TABLE_TITLE);
			resultSet = statement.executeQuery(query.toString());
			entries = UserDao.OBJECT_MAPPER.map(resultSet, UserDao.MAPPING_FIELDS);
			
		} finally {
			if(statement != null) statement.close();
			if(resultSet != null) resultSet.close();
		}
		
		return entries;
	}
	
	@Override
	public synchronized int create(User instance) throws SQLException {
		
		if(instance == null)
			throw new NullPointerException("instance == null");
		
		StringBuilder query = new StringBuilder("INSERT INTO ");
		query.append(User.TABLE_TITLE);
		query.append(" SET ");
		
		query.append(User.NAME_COLUMN);
		query.append("=?,");
		query.append(User.SURNAME_COLUMN);
		query.append("=?,");
		query.append(User.BIRTHDATE_COLUMN);
		query.append("=?");
		
		PreparedStatement statement = null;
		
		try {
		
			statement =	super.connection.prepareStatement(query.toString());
			statement.setString(1, instance.getName());
			statement.setString(2, instance.getSurname());
			statement.setLong(3, instance.getBirthdate());
			
			return statement.executeUpdate();
			
		} finally {
			if(statement != null) statement.close();
		}
	}

	@Override
	public synchronized User read(int id) throws SQLException {
		
		StringBuilder query = new StringBuilder("SELECT * FROM ");
		query.append(User.TABLE_TITLE);
		query.append(" WHERE ");
		query.append(User.ID_COLUMN);
		query.append("=? LIMIT 1");
		
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		Collection<User> entries = null;
		
		try {
			statement = super.connection.prepareStatement(query.toString());
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			entries = UserDao.OBJECT_MAPPER.map(resultSet, UserDao.MAPPING_FIELDS);
			
		} finally {
			if(statement != null) statement.close();
			if(resultSet != null) resultSet.close();
		}
		
		// Cannot be more than 1
		// result with the same id
		Assert.assertFalse("multiple rows with such id exist", entries.size() > 1);
		
		/* 
		 * getting the first object from
		 * the collection of entries
		 * */
		User [] a = new User[1];
		entries.toArray(a);
		
		return a[0];
	}

	@Override
	public synchronized int update(User instance) throws SQLException {
		
		if(instance == null)
			throw new NullPointerException("instance == null");
		
		StringBuilder query = new StringBuilder("UPDATE ");
		query.append(User.TABLE_TITLE);
		query.append(" SET ");
		
		query.append(User.NAME_COLUMN);
		query.append("=?,");
		query.append(User.SURNAME_COLUMN);
		query.append("=?,");
		query.append(User.BIRTHDATE_COLUMN);
		query.append("=?");
		
		query.append(" WHERE ");
		query.append(User.ID_COLUMN);
		query.append("=? LIMIT 1");
		
		PreparedStatement statement = null;
		
		try {
		
			statement =	super.connection.prepareStatement(query.toString());
			statement.setString(1, instance.getName());
			statement.setString(2, instance.getSurname());
			statement.setLong(3, instance.getBirthdate());
			
			return statement.executeUpdate();
			
		} finally {
			if(statement != null) statement.close();
		}
		
	}

	@Override
	public synchronized int delete(User instance) throws SQLException {
		
		if(instance == null)
			throw new NullPointerException("instance == null");
		
		StringBuilder query = new StringBuilder("DELETE FROM ");
		query.append(User.TABLE_TITLE);
		query.append(" WHERE ");
		query.append(User.ID_COLUMN);
		query.append("=? LIMIT 1");
		
		PreparedStatement statement = null;
		
		try {
		
			statement =	super.connection.prepareStatement(query.toString());
			statement.setLong(1, instance.getReaderCard());
			
			return statement.executeUpdate();
			
		} finally {
			if(statement != null) statement.close();
		}
	}
	
}
