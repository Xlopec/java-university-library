package com.ua.max.oliynick.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Assert;

import com.ua.max.oliynick.interfaces.Dao;
import com.ua.max.oliynick.interfaces.ObjectMapper;

/**
 * Implementation of {@link Dao} class
 * for {@link Author} class
 * @author Max Oliynick
 * @version 1.0
 * */

public class AuthorDao extends Dao < Author > {
	
	/**Fields to map database object*/
	public final static String[] MAPPING_FIELDS = new String [] 
			{
			Author.ID_COLUMN,
			Author.NAME_COLUMN,
			Author.SURNAME_COLUMN,
			Author.BIRTHDATE_COLUMN
			};
	
	// Maps database request into java object
	private final static ObjectMapper<Author> OBJECT_MAPPER = new ObjectMapper<Author>() {
		
		public List<Author> map(final ResultSet rs, final String [] columns) throws SQLException {
			
			List<Author> items = new ArrayList<Author>();
	    	
	    	while(rs.next()) {
	    		
	    		Author item = new Author();
	    		
	    		for(int i = 0; i < columns.length; ++i) {
	    			if(columns[i].equals(Author.ID_COLUMN)) {
	        			item.setId(rs.getInt(Author.ID_COLUMN));
	        			
	        		} else if(columns[i].equalsIgnoreCase(Author.NAME_COLUMN)) {
	        			item.setName(rs.getString(Author.NAME_COLUMN));
	        			
	        		} else if(columns[i].equalsIgnoreCase(Author.SURNAME_COLUMN)) {
	        			item.setSurname(rs.getString(Author.SURNAME_COLUMN));
	        			
	        		} else if(columns[i].equalsIgnoreCase(Author.BIRTHDATE_COLUMN)) {
	        			item.setBirthdate(rs.getLong(Author.BIRTHDATE_COLUMN));
	        			
	        		}
	    		}
	    		
	    		items.add(item);
	    	}
	    	
	    	return items;
		}
	};


	/**Creates dao for {@link Author} class
	 * @param connection - database connection to use
	 * @param mapper - mapper to map objects*/
	public AuthorDao(final Connection connection) {
		super(connection);
	}

	@Override
	public synchronized List<Author> all() throws SQLException {
		
		StringBuilder query = new StringBuilder("SELECT * FROM ");
		query.append(Author.TABLE_TITLE);
		
		Statement statement = null;
		ResultSet resultSet = null;
		List<Author> entries = null;
		
		try {
			statement = super.connection.createStatement();
			resultSet = statement.executeQuery(query.toString());
			entries = AuthorDao.OBJECT_MAPPER.map(resultSet, AuthorDao.MAPPING_FIELDS);
			
		} finally {
			if(statement != null) statement.close();
			if(resultSet != null) resultSet.close();
		}
		
		return entries;
	}
	
	@Override
	public synchronized int create(Author instance) throws SQLException {
		
		if(instance == null)
			throw new NullPointerException("instance == null");
		
		StringBuilder query = new StringBuilder("INSERT INTO ");
		query.append(Author.TABLE_TITLE);
		query.append(" SET ");
		
		query.append(Author.NAME_COLUMN);
		query.append("=?,");
		query.append(Author.SURNAME_COLUMN);
		query.append("=?,");
		query.append(Author.BIRTHDATE_COLUMN);
		query.append("=?");
		
		PreparedStatement statement = null;
		
		try {
		
			statement =	super.connection.prepareStatement(query.toString());
			statement.setString(1, instance.getName());
			statement.setString(2, instance.getSurname());
			statement.setLong(3, instance.getBirthdate());
			
			return statement.executeUpdate();
			
		} finally {
			if(statement != null) statement.close();
		}
	}

	@Override
	public synchronized Author read(int id) throws SQLException {
		
		StringBuilder query = new StringBuilder("SELECT * FROM ");
		query.append(Author.TABLE_TITLE);
		query.append(" WHERE ");
		query.append(Author.ID_COLUMN);
		query.append("=? LIMIT 1");
		
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		Collection<Author> entries = null;
		
		try {
			statement = super.connection.prepareStatement(query.toString());
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			entries = AuthorDao.OBJECT_MAPPER.map(resultSet, AuthorDao.MAPPING_FIELDS);
			
		} finally {
			if(statement != null) statement.close();
			if(resultSet != null) resultSet.close();
		}
		
		// Cannot be more than 1
		// result with the same id
		Assert.assertFalse("multiple rows with such id exist", entries.size() > 1);
		
		/* 
		 * getting the first object from
		 * the collection of entries
		 * */
		Author [] a = new Author[1];
		entries.toArray(a);
		
		return a[0];
	}

	@Override
	public synchronized int update(Author instance) throws SQLException {
		
		if(instance == null)
			throw new NullPointerException("instance == null");
		
		StringBuilder query = new StringBuilder("UPDATE ");
		query.append(Author.TABLE_TITLE);
		query.append(" SET ");
		
		query.append(Author.NAME_COLUMN);
		query.append("=?,");
		query.append(Author.SURNAME_COLUMN);
		query.append("=?,");
		query.append(Author.BIRTHDATE_COLUMN);
		query.append("=?");
		
		query.append(" WHERE ");
		query.append(Author.ID_COLUMN);
		query.append("=? LIMIT 1");
		
		PreparedStatement statement = null;
		
		try {
		
			statement =	super.connection.prepareStatement(query.toString());
			statement.setString(1, instance.getName());
			statement.setString(2, instance.getSurname());
			statement.setLong(3, instance.getBirthdate());
			statement.setInt(4, instance.getId());
			
			return statement.executeUpdate();
			
		} finally {
			if(statement != null) statement.close();
		}
		
	}

	@Override
	public synchronized int delete(Author instance) throws SQLException {
		
		if(instance == null)
			throw new NullPointerException("instance == null");
		
		StringBuilder query = new StringBuilder("DELETE FROM ");
		query.append(Author.TABLE_TITLE);
		query.append(" WHERE ");
		query.append(Author.ID_COLUMN);
		query.append("=? LIMIT 1");
		
		PreparedStatement statement = null;
		
		try {
		
			statement =	super.connection.prepareStatement(query.toString());
			statement.setInt(1, instance.getId());
			
			return statement.executeUpdate();
			
		} finally {
			if(statement != null) statement.close();
		}
	}
	
}
