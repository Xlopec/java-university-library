package com.ua.max.oliynick.database;

import java.io.Serializable;

/**
 * This class represents
 * database table, name
 * of which "author"
 * @version 1.0
 * @author Max Oliynick
 * */

public final class Author implements Serializable {
	
	private static final long serialVersionUID = 8500107530633194144L;
	
	/**name of the database table*/
	public static final String TABLE_TITLE = "author";
	
	public static final String ID_COLUMN = "idAuthor";
	public static final String NAME_COLUMN = "name";
	public static final String SURNAME_COLUMN = "surname";
	public static final String BIRTHDATE_COLUMN = "birthdate";
	
	private int id;
	private String name;
	private String surname;
	private long birthdate;
	
	public Author() {}
	
	public Author(String name, String surname, long birthdate) {
		super();
		this.name = name;
		this.surname = surname;
		this.birthdate = birthdate;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) return false;
		if(!(obj instanceof Order)) return false;
		
		Author another = (Author) obj;
		
		if(another.getId() != getId()) return false;
		
		if(another.getName() != null && getName() != null) {
			if(!another.getName().equals(getName())) return false;
		}
		
		if(another.getSurname() != null && getSurname() != null) {
			if(!another.getSurname().equals(getSurname())) return false;
		}
		if(another.getBirthdate() != getBirthdate()) return false;
		
		return super.equals(another);
	}

	@Override
	public int hashCode() {
		return id * 11;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public long getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(long birthdate) {
		this.birthdate = birthdate;
	}

	@Override
	public String toString() {
		return String.valueOf(getId()) + ' '
				+ getName() + ' '
				+ getSurname() + ' '
				+ String.valueOf(getBirthdate());
	}

}
