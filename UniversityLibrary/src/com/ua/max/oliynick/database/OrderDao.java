package com.ua.max.oliynick.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Assert;

import com.ua.max.oliynick.interfaces.Dao;
import com.ua.max.oliynick.interfaces.ObjectMapper;

/**
 * Implementation of {@link Dao} class
 * for {@link Order} class
 * @author Max Oliynick
 * @version 1.0
 * */

public class OrderDao extends Dao < Order > {
	
	/**Fields to map database object*/
	public final static String[] MAPPING_FIELDS = new String [] 
			{
			Order.ID_COLUMN,
			Order.SINCE_DATE_COLUMN,
			Order.TO_DATE_COLUMN,
			Order.REAL_DATE_COLUMN,
			Order.BOOK_REF_COLUMN,
			Order.USER_REF_COLUMN
			};
	
	// Maps database request into java object
	private final static ObjectMapper<Order> OBJECT_MAPPER = new ObjectMapper<Order>() {
		
		public List<Order> map(final ResultSet rs, final String [] columns) throws SQLException {
			
			List<Order> items = new ArrayList<Order>();
	    	
	    	while(rs.next()) {
	    		
	    		Order item = new Order();
	    		
	    		for(int i = 0; i < columns.length; ++i) {
	    			if(columns[i].equals(Order.ID_COLUMN)) {
	        			item.setId(rs.getInt(Order.ID_COLUMN));
	        			
	        		} else if(columns[i].equalsIgnoreCase(Order.SINCE_DATE_COLUMN)) {
	        			item.setSinceDate(rs.getLong(Order.SINCE_DATE_COLUMN));
	        			
	        		} else if(columns[i].equalsIgnoreCase(Order.TO_DATE_COLUMN)) {
	        			item.setToDate(rs.getLong(Order.TO_DATE_COLUMN));
	        			
	        		} else if(columns[i].equalsIgnoreCase(Order.REAL_DATE_COLUMN)) {
	        			item.setRealDate(rs.getLong(Order.REAL_DATE_COLUMN));
	        			
	        		} else if(columns[i].equalsIgnoreCase(Order.BOOK_REF_COLUMN)) {
	        			BookInstance bookRef = new BookInstance();
	        			bookRef.setInventoryNum(rs.getInt(Order.BOOK_REF_COLUMN));
	        			item.setBookInstance(bookRef);
	        			
	        		} else if(columns[i].equalsIgnoreCase(Order.USER_REF_COLUMN)) {
	        			User userRef = new User();
	        			userRef.setReaderCard(rs.getInt(Order.USER_REF_COLUMN));
	        			item.setUser(userRef);
	        			
	        		}
	    		}
	    		
	    		items.add(item);
	    	}
	    	
	    	return items;
		}
	};


	/**Creates dao for {@link Order} class
	 * @param connection - database connection to use
	 * @param mapper - mapper to map objects*/
	public OrderDao(final Connection connection) {
		super(connection);
	}

	@Override
	public synchronized List<Order> all() throws SQLException {
		
		StringBuilder query = new StringBuilder("SELECT * FROM ");
		query.append(Order.TABLE_TITLE);
		
		Statement statement = null;
		ResultSet resultSet = null;
		List<Order> entries = null;
		
		try {
			statement = super.connection.createStatement();
			resultSet = statement.executeQuery(query.toString());
			entries = OrderDao.OBJECT_MAPPER.map(resultSet, OrderDao.MAPPING_FIELDS);
			
		} finally {
			if(statement != null) statement.close();
			if(resultSet != null) resultSet.close();
		}
		
		return entries;
	}
	
	@Override
	public synchronized int create(Order instance) throws SQLException {
		
		if(instance == null)
			throw new NullPointerException("instance == null");
		
		StringBuilder query = new StringBuilder("INSERT INTO ");
		query.append(Order.TABLE_TITLE);
		query.append(" SET ");
		
		query.append(Order.SINCE_DATE_COLUMN);
		query.append("=?,");
		query.append(Order.TO_DATE_COLUMN);
		query.append("=?,");
		query.append(Order.REAL_DATE_COLUMN);
		query.append("=?,");
		query.append(Order.BOOK_REF_COLUMN);
		query.append("=?,");
		query.append(Order.USER_REF_COLUMN);
		query.append("=?");
		
		PreparedStatement statement = null;
		
		try {
		
			statement =	super.connection.prepareStatement(query.toString());
			statement.setLong(1, instance.getSince());
			statement.setLong(2, instance.getTo());
			statement.setLong(3, instance.getRealDate());
			statement.setInt(4, instance.getBookInstance().getInventoryNum());
			statement.setInt(5, instance.getUser().getReaderCard());
			
			return statement.executeUpdate();
			
		} finally {
			if(statement != null) statement.close();
		}
	}

	@Override
	public synchronized Order read(int id) throws SQLException {
		
		StringBuilder query = new StringBuilder("SELECT * FROM ");
		query.append(Order.TABLE_TITLE);
		query.append(" WHERE ");
		query.append(Order.ID_COLUMN);
		query.append("=? LIMIT 1");
		
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		Collection<Order> entries = null;
		
		try {
			statement = super.connection.prepareStatement(query.toString());
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			entries = OrderDao.OBJECT_MAPPER.map(resultSet, OrderDao.MAPPING_FIELDS);
			
		} finally {
			if(statement != null) statement.close();
			if(resultSet != null) resultSet.close();
		}
		
		// Cannot be more than 1
		// result with the same id
		Assert.assertFalse("multiple rows with such id exist", entries.size() > 1);
		
		/* 
		 * getting the first object from
		 * the collection of entries
		 * */
		Order [] a = new Order[1];
		entries.toArray(a);
		
		return a[0];
	}

	@Override
	public synchronized int update(Order instance) throws SQLException {
		
		if(instance == null)
			throw new NullPointerException("instance == null");
		
		StringBuilder query = new StringBuilder("UPDATE ");
		query.append(Order.TABLE_TITLE);
		query.append(" SET ");
		
		query.append(Order.SINCE_DATE_COLUMN);
		query.append("=?,");
		query.append(Order.TO_DATE_COLUMN);
		query.append("=?,");
		query.append(Order.REAL_DATE_COLUMN);
		query.append("=?,");
		query.append(Order.BOOK_REF_COLUMN);
		query.append("=?,");
		query.append(Order.USER_REF_COLUMN);
		query.append("=?");
		
		query.append(" WHERE ");
		query.append(Order.ID_COLUMN);
		query.append("=? LIMIT 1");
		
		PreparedStatement statement = null;
		
		try {
		
			statement =	super.connection.prepareStatement(query.toString());
			statement.setLong(1, instance.getSince());
			statement.setLong(2, instance.getTo());
			statement.setLong(3, instance.getRealDate());
			statement.setInt(4, instance.getBookInstance().getInventoryNum());
			statement.setInt(5, instance.getUser().getReaderCard());
			
			return statement.executeUpdate();
			
		} finally {
			if(statement != null) statement.close();
		}
		
	}

	@Override
	public synchronized int delete(Order instance) throws SQLException {
		
		if(instance == null)
			throw new NullPointerException("instance == null");
		
		StringBuilder query = new StringBuilder("DELETE FROM ");
		query.append(Order.TABLE_TITLE);
		query.append(" WHERE ");
		query.append(Order.ID_COLUMN);
		query.append("=? LIMIT 1");
		
		PreparedStatement statement = null;
		
		try {
		
			statement =	super.connection.prepareStatement(query.toString());
			statement.setInt(1, instance.getId());
			
			return statement.executeUpdate();
			
		} finally {
			if(statement != null) statement.close();
		}
	}
	
}
