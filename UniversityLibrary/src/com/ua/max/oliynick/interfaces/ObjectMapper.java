package com.ua.max.oliynick.interfaces;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * This class stands for transforming
 * database response into java objects.
 * T represents class to be used
 * @version 1.0
 * @author Max Oliynick
 * */

public abstract interface ObjectMapper <T extends Serializable> {

	/** Maps result set into set of java objects
	 * @param rs - result set to be transformed
	 * @param columns - database column names
	 * @throws SQLException if error occurs during this operation
	 * */
	public List<T> map(final ResultSet rs, final String [] columns) throws SQLException;
	
}
