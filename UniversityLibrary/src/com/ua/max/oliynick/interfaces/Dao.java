package com.ua.max.oliynick.interfaces;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * This class represents structure of DAO object.
 * T represents corresponding bean
 *  of this class
 * @version 1.0
 * @author Max Oliynick
 * */

public abstract class Dao <T extends Serializable> {
	
	/**Connection representation*/
	protected final Connection connection;
	
	/**Creates dao for specified class T using special mapper M
	 * @param connection - database connection to use
	 * @param mapper - mapper to map objects
	 * @throws NullPointerException if any of parameters is null*/
	public Dao(final Connection connection) {
		
		if(connection == null)
			throw new NullPointerException("connection == null");
		
		this.connection = connection;
	}
	
	/**Returns current connection object*/
	public Connection getConnection() {
		return connection;
	}

	/**Tries to close current connection
	 * @throws SQLException if error occurs during this operation*/
	public void closeConnection() throws SQLException {
		connection.close();
	}

	/**
	 * Returns all objects which are associated
	 * with rows of the table
	 * @throws SQLException if error occurs during this operation
	 * */
	public abstract List<T> all() throws SQLException;
	
	/**
	 * Inserts new row in the database 
	 * associated with this id
	 * @param instance - object to be inserted
	 * @return amount of affected rows
	 * @throws SQLException if error occurs during this operation
	 * */
	public abstract int create(T instance) throws SQLException;
	
	/**
	 * Finds and returns row from database 
	 * associated with this id
	 * @param id - unique id of the object
	 * @throws SQLException if error occurs during this operation
	 * */
	public abstract T read(int id) throws SQLException;
	
	/**
	 * Finds and updates row in database 
	 * associated with this object
	 * @param instance - object to update
	 * @return amount of affected rows
	 * @throws SQLException if error occurs during this operation
	 * */
	public abstract int update(T instance) throws SQLException;
	
	/**
	 * Finds and deletes row in database 
	 * associated with this object
	 * @param instance - object to delete
	 * @return amount of affected rows
	 * @throws SQLException if error occurs during this operation
	 * */
	public abstract int delete(T instance) throws SQLException;
	
}
