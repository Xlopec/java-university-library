package com.ua.max.oliynick.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.ua.max.oliynick.database.Author;
import com.ua.max.oliynick.database.AuthorDao;
import com.ua.max.oliynick.database.Book;
import com.ua.max.oliynick.database.BookDao;
import com.ua.max.oliynick.database.BookInstance;
import com.ua.max.oliynick.database.BookInstanceDao;
import com.ua.max.oliynick.database.Order;
import com.ua.max.oliynick.database.OrderDao;
import com.ua.max.oliynick.database.User;
import com.ua.max.oliynick.database.UserDao;
import com.ua.max.oliynick.interfaces.Dao;

public class DaoTester {
	
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	static final String DB_URL = "jdbc:mysql://localhost:3306/dbexample";
	
	public static void main(String [] args) throws SQLException {
		
		try {
			Class.forName(JDBC_DRIVER);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		Connection connection = DriverManager.getConnection(DB_URL, "", "");
			
			Dao< Author > daoAuthor = 
					new AuthorDao(
							connection
							);
			
			Dao< Book > daoBook = 
					new BookDao(
							connection
							);
			
			Dao< BookInstance > daoBookInstance = 
					new BookInstanceDao(
							connection
							);
			
			Dao< Order > daoOrder = 
					new OrderDao(
							connection
							);
			
			Dao< User > daoUser = 
					new UserDao(
							connection
							);
			
			System.out.println(daoAuthor.all());
			System.out.println(daoBook.all());
			System.out.println(daoBookInstance.all());
			System.out.println(daoOrder.all());
			System.out.println(daoUser.all());
					
	}

}
