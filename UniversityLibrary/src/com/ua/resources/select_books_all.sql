select 
book.*, 
count(book_instance.Book_ISBN) as available_num 
from book 
left join book_instance 
on (book_instance.Book_ISBN = book.ISBN) 
group by book.ISBN